package m.GIQ.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import m.GIQ.Items.Question;
import m.GIQ.QuizHelper;
import m.GIQ.R;

/**
 * Created by husic on 26.11.2016..
 */
public class FirstActivity extends Activity implements View.OnClickListener {

    static ImageButton goB;
    //Views
    ImageButton exitB, infoB, optionsB;
    Dialog difficultyDialog;

    QuizHelper quizHelper;
    Thread threadfirst;
    boolean score = false;
    //Ads
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstactivity);

        //Find views
        mAdView = (AdView) findViewById(R.id.adView);
        goB = (ImageButton) findViewById(R.id.goB);
        exitB = (ImageButton) findViewById(R.id.scoreB);
        infoB = (ImageButton) findViewById(R.id.infoB);
        optionsB = (ImageButton) findViewById(R.id.optionsB);

        //Set up ads
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        quizHelper = new QuizHelper(QuestionActivity.NETWORK_KEY, this);

        goB.setOnClickListener(this);
        exitB.setOnClickListener(this);
        infoB.setOnClickListener(this);
        optionsB.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        if (mAdView != null)
            mAdView.pause();

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (threadfirst != null)
            threadfirst.interrupt();

        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mAdView != null)
            mAdView.resume();
    }

    @Override
    public void onDestroy() {
        if (mAdView != null)
            mAdView.destroy();

        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scoreB:
                score = true;
            case R.id.goB:
                //Quiz should start, so to fetch questions we need some time, and we
                //make spinner to fill the space, and then we catch the questions

                difficultyDialog = new Dialog(this);
                difficultyDialog.setContentView(R.layout.dialog_difficulty);
                difficultyDialog.setTitle("Choose difficulty");

                difficultyDialog.findViewById(R.id.easyButton).setOnClickListener(this);
                difficultyDialog.findViewById(R.id.mediumButton).setOnClickListener(this);
                difficultyDialog.findViewById(R.id.hardButton).setOnClickListener(this);

                difficultyDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        score = false;
                    }
                });

                difficultyDialog.show();

                break;
            case R.id.easyButton:
            case R.id.mediumButton:
            case R.id.hardButton:

                final int difficultyLevel;
                if (v.getId() == R.id.easyButton)
                    difficultyLevel = 1;
                else if (v.getId() == R.id.mediumButton)
                    difficultyLevel = 2;
                else
                    difficultyLevel = 3;

                difficultyDialog.dismiss();

                if (score) {
                    Intent scoreActivity = new Intent(FirstActivity.this, HighScore.class);
                    scoreActivity.putExtra("difficulty", difficultyLevel);
                    startActivity(scoreActivity);

                    score = false;
                } else {
                    final ProgressDialog nDialog = new ProgressDialog(FirstActivity.this);
                    nDialog.setMessage("Loading..");
                    nDialog.setTitle("Getting Data");
                    nDialog.setIndeterminate(true);
                    nDialog.setCancelable(false);
                    nDialog.setCanceledOnTouchOutside(false);
                    nDialog.show();

                    threadfirst = new Thread(new Runnable() {
                        public void run() {
                            while (!quizHelper.questionReady && !quizHelper.questionError)
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {

                                }

                            if (!quizHelper.questionError && quizHelper.hasDifficulty(difficultyLevel)) {
                                Intent intent = new Intent(FirstActivity.this, QuestionActivity.class);

                                ArrayList<Question> questions = new ArrayList<>();
                                for (Question q : quizHelper.questions)
                                    if (q.getDifficulty() == difficultyLevel)
                                        questions.add(q);

                                Bundle args = new Bundle();
                                args.putSerializable("ARRAYLIST", questions);
                                args.putInt("difficulty", difficultyLevel);
                                intent.putExtra("BUNDLE", args);

                                startActivity(intent);
                            } else
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(FirstActivity.this, "No questions. Please connect to network!", Toast.LENGTH_LONG).show();
                                    }
                                });

                            nDialog.dismiss();
                        }
                    });

                    threadfirst.start();
                }
                break;
            case R.id.infoB:
                Intent infoActivity = new Intent(FirstActivity.this, InfoActivity.class);
                startActivity(infoActivity);
                break;
            case R.id.optionsB:
                Intent settingsActivity = new Intent(FirstActivity.this, SettingsActivity.class);
                startActivity(settingsActivity);
                finish();
                break;
        }
    }
}
