package m.GIQ.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import m.GIQ.R;

public class SettingsActivity extends Activity implements View.OnClickListener {

    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int REQUEST_CODE = 1001;

    private AdView mAdView;
    private ImageButton donateButton;

    ArrayList<String> skuList = new ArrayList<>();

    IInAppBillingService mService;
    ServiceConnection mServiceConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        donateButton = (ImageButton) findViewById(R.id.donateButton);
        donateButton.setOnClickListener(this);
        donateButton.setVisibility(View.GONE);

        Button logoutButton = (Button) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(this);

        mServiceConn = new MyServiceConnection();

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.donateButton:
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
                builderSingle.setIcon(R.drawable.islamlogo);
                builderSingle.setTitle("Select Donation value:");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add("Donation 0.5€");
                arrayAdapter.add("Donation 1€");
                arrayAdapter.add("Donation 2€");
                arrayAdapter.add("Donation 5€");
                arrayAdapter.add("Donation 10€");

                builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        initiateDonation(which);
                    }
                });
                builderSingle.show();
                break;
            case R.id.logout_button:
                LoginManager.getInstance().logOut();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove("name");
                editor.commit();

                Intent intent = new Intent(SettingsActivity.this, LoginPage.class);
                startActivity(intent);

                finish();
                break;
        }
    }

    private void initiateDonation(int index) {

        skuList.add("donation_0.5");
        skuList.add("donation_1");
        skuList.add("donation_2");
        skuList.add("donation_5");
        skuList.add("donation_10");
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

        try {
            Bundle skuDetails = mService.getSkuDetails(3, getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == BILLING_RESPONSE_RESULT_OK) {

                Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
                    skuList.get(index), "inapp", "");

                PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                startIntentSenderForResult(pendingIntent.getIntentSender(),
                    REQUEST_CODE, new Intent(), 0, 0, 0);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");

                    onDonated(skuList.indexOf(sku));

                    Log.d("Billing", "You have bought the " + sku + ". Excellent choice, adventurer!");
                } catch (JSONException e) {
                    Log.d("Billing", "Failed to parse purchase data.");
                    e.printStackTrace();
                }
            }
        }
    }

    public void onDonated(int amountIndex) {
        Toast.makeText(this, "Donated: " + skuList.get(amountIndex), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, FirstActivity.class);
        startActivity(intent);
        finish();
    }

    class MyServiceConnection implements ServiceConnection {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            donateButton.setVisibility(View.GONE);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            donateButton.setVisibility(View.VISIBLE);
        }
    }
}
