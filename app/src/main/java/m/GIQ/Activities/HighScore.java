package m.GIQ.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

import m.GIQ.Database.OnDatabaseDone;
import m.GIQ.Database.ScoresGetter;
import m.GIQ.Items.Score;
import m.GIQ.R;
import m.GIQ.ScoreAdapter;

/**
 * Created by mersad on 2/13/2016.
 */
public class HighScore extends Activity implements OnDatabaseDone<List<Score>> {

    public List<Score> itemList = new ArrayList<>();
    protected RecyclerView recyclerView;
    protected ScoreAdapter mAdapter;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_score);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new ScoreAdapter(itemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        int difficulty = getIntent().getIntExtra("difficulty", 0);

        new ScoresGetter(QuestionActivity.NETWORK_KEY, difficulty).execute(this);
    }

    @Override
    public void OnDatabaseGetData(List<Score> data) {
        for (Score score : data)
            itemList.add(score);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}

