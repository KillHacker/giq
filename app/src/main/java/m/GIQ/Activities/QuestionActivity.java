package m.GIQ.Activities;

/**
 * Created by H on 7/12/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import m.GIQ.App;
import m.GIQ.Database.OnDatabaseDone;
import m.GIQ.Database.ScoreSetter;
import m.GIQ.Items.Question;
import m.GIQ.QuizHelper;
import m.GIQ.R;

public class QuestionActivity extends Activity implements OnDatabaseDone<List<Boolean>>, View.OnClickListener {

    public final static String NETWORK_KEY = "H72jsUahGT2sadhjb";

    QuizHelper quizHelper;

    //Quiz stats
    int difficulty;
    Question currentQ;
    int score = 0;
    int mProgressStatus = 0;
    boolean progressInterrupt = false;
    private boolean bPolaUsed = false, bCetvrtinaUsed = false, bPozivUsed = false, bZamjenaUsed = false;
    //Views
    private TextView txtQuestion, scored;
    private AppCompatButton button1, button2, button3, button4;
    private AppCompatButton bpola, bCetvrtina, bpoziv, bzamjena;
    private ProgressBar mProgress;
    private AlertDialog dialog;
    private AdView mAdView;
    private volatile Thread progressThread;
    private Handler handler;
    private boolean pauseTimerForAnswerShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //Find views
        mAdView = (AdView) findViewById(R.id.adView);
        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        button1 = (AppCompatButton) findViewById(R.id.button1);
        button2 = (AppCompatButton) findViewById(R.id.button2);
        button3 = (AppCompatButton) findViewById(R.id.button3);
        button4 = (AppCompatButton) findViewById(R.id.button4);
        bpola = (AppCompatButton) findViewById(R.id.bPola);
        bCetvrtina = (AppCompatButton) findViewById(R.id.bCetvrtina);
        bpoziv = (AppCompatButton) findViewById(R.id.bPoziv);
        bzamjena = (AppCompatButton) findViewById(R.id.bZamjena);
        mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        scored = (TextView) findViewById(R.id.score);

        //Personalize
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/droid.ttf");
        txtQuestion.setTypeface(type);
        button1.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button2.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button3.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        button4.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        bpola.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        bpoziv.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        bzamjena.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
        bCetvrtina.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);

        //Load ad
        AdRequest adRequest = new AdRequest.Builder()
            .build();
        mAdView.loadAd(adRequest);

        handler = new Handler();

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        bpola.setOnClickListener(this);
        bCetvrtina.setOnClickListener(this);
        bpoziv.setOnClickListener(this);
        bzamjena.setOnClickListener(this);

        //Prepare questions
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");

        ArrayList<Question> questions = (ArrayList<Question>) args.getSerializable("ARRAYLIST");
        quizHelper = new QuizHelper(questions, this);

        difficulty = args.getInt("difficulty");

        //Start the game
        startTheTimer();
        SetQuestion();
    }

    private void resetGameAndStart() {
        bPolaUsed = false;
        bPozivUsed = false;
        bZamjenaUsed = false;
        bCetvrtinaUsed = false;
        bpola.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        bpoziv.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        bzamjena.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        bCetvrtina.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        UpdateScore(0);
        SetQuestion();
        mProgressStatus = 0;
        progressInterrupt = false;

        startTheTimer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                Answer(0, true);
                break;
            case R.id.button2:
                Answer(1, true);
                break;
            case R.id.button3:
                Answer(2, true);
                break;
            case R.id.button4:
                Answer(3, true);
                break;
            case R.id.bPola:
                bPolaUsed = true;
                HelpHalf();
                bpola.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bpola.setEnabled(false);
                break;
            case R.id.bCetvrtina:
                bCetvrtinaUsed = true;
                LeaveOnlyRightAnswer();
                bCetvrtina.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bCetvrtina.setEnabled(false);
                break;
            case R.id.bPoziv:
                bPozivUsed = true;
                LeaveOnlyRightAnswer();
                bpoziv.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bpoziv.setEnabled(false);
                break;
            case R.id.bZamjena:
                bZamjenaUsed = true;
                SkipQuestion();
                bzamjena.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.used_help)));
                bzamjena.setEnabled(false);
                break;
        }
    }

    private void HelpHalf() {
        int a = currentQ.getRightAnswer();

        Random random = new Random();
        int b = random.nextInt(4);
        if (a == b)
            b++;
        if (b == 4)
            b = 0;

        if (a != 0 && b != 0)
            button1.setText("");
        if (a != 1 && b != 1)
            button2.setText("");
        if (a != 2 && b != 2)
            button3.setText("");
        if (a != 3 && b != 3)
            button4.setText("");
    }

    private void LeaveOnlyRightAnswer() {
        int a = currentQ.getRightAnswer();

        if (a != 0) {
            button1.setText("");
            button1.setEnabled(false);
        }
        if (a != 1) {
            button2.setText("");
            button2.setEnabled(false);
        }
        if (a != 2) {
            button3.setText("");
            button3.setEnabled(false);
        }
        if (a != 3) {
            button4.setText("");
            button4.setEnabled(false);
        }
    }

    private void SkipQuestion() {
        Answer(currentQ.getRightAnswer(), false);
    }

    private void startTheTimer() {
        if (difficulty != 3)
            return;

        progressThread = new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus <= 100 && !progressInterrupt) {
                    if (!pauseTimerForAnswerShowing) {
                        mProgressStatus++;
                        mProgress.setProgress(mProgressStatus);
                    }
                    try {
                        Thread.sleep(200);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!progressInterrupt)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Answer(-1, false);
                        }
                    });
            }
        });
        progressInterrupt = false;
        progressThread.start();

        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (mProgressStatus < 100) {
            mProgressStatus = 100;
            mProgress.setProgress(mProgressStatus);
            progressInterrupt = true;
            GameOver();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void OnDatabaseGetData(List<Boolean> data) {
        //*Score saved to base*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = new AlertDialog.Builder(QuestionActivity.this).create();
                dialog.setMessage("Saved!");
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "New game",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resetGameAndStart();
                        }
                    });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Main menu",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                dialog.show();
            }
        });
    }

    public void SetQuestion() {
        Question nextQ = quizHelper.GetRandomQuestion();

        if (nextQ != null) {
            currentQ = nextQ;
            setQuestionViews();
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(QuestionActivity.this, "Congratulations, you answered all questions from this level!", Toast.LENGTH_LONG).show();
                }
            });
            GameOver();
        }
    }

    public void Answer(int answeredId, boolean increaseScore) {
        AppCompatButton button;
        switch (answeredId) {
            case 0:
                button = button1;
                break;
            case 1:
                button = button2;
                break;
            case 2:
                button = button3;
                break;
            default:
                button = button4;
                break;
        }

        AppCompatButton rightButton;
        switch (currentQ.getRightAnswer()) {
            case 0:
                rightButton = button1;
                break;
            case 1:
                rightButton = button2;
                break;
            case 2:
                rightButton = button3;
                break;
            default:
                rightButton = button4;
                break;
        }

        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
        button4.setEnabled(false);
        bpola.setEnabled(false);
        bpoziv.setEnabled(false);
        bzamjena.setEnabled(false);
        bCetvrtina.setEnabled(false);

        pauseTimerForAnswerShowing = true;

        if (currentQ.getRightAnswer() == answeredId) {
            button.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));

            if (increaseScore)
                UpdateScore(score + 1);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mProgressStatus = 0;
                    SetQuestion();
                }
            }, 800);
        } else if (answeredId == -1) {
            rightButton.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GameOver();
                }
            }, 800);
        } else {
            rightButton.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.right_answer)));
            button.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.false_answer)));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GameOver();
                }
            }, 800);
        }
    }

    public void GameOver() {
        //Stop the timer
        progressInterrupt = true;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = new AlertDialog.Builder(QuestionActivity.this).create();
                dialog.setMessage("The right answer is: " + currentQ.getRightText());
                dialog.setTitle("Total points: " + score);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "New game",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resetGameAndStart();
                        }
                    });
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Main menu",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                if (score > 0)
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String fbUserId = "";
                                try {
                                    fbUserId = Profile.getCurrentProfile().getId();
                                    if (fbUserId == null || fbUserId.isEmpty())
                                        fbUserId = "";
                                } catch (NullPointerException e) {
                                }
                                ScoreSetter scoreSetter = new ScoreSetter(NETWORK_KEY, App.getInstance().name, score, difficulty, fbUserId);
                                scoreSetter.execute(QuestionActivity.this);
                            }
                        });
                dialog.show();
            }
        });
    }

    private void UpdateScore(int score) {
        this.score = score;
        scored.setText("" + score);
    }

    private void setQuestionViews() {
        txtQuestion.setText(currentQ.getQuestionText());

        String[] texts = currentQ.Randomize();

        button1.setText(texts[0]);
        button2.setText(texts[1]);
        button3.setText(texts[2]);
        button4.setText(texts[3]);

        button1.setEnabled(true);
        button2.setEnabled(true);
        button3.setEnabled(true);
        button4.setEnabled(true);

        button1.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button2.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button3.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        button4.setSupportBackgroundTintList(ColorStateList.valueOf(Color.WHITE));

        bpola.setEnabled(!bPolaUsed);
        bpoziv.setEnabled(!bPozivUsed);
        bzamjena.setEnabled(!bZamjenaUsed);
        bCetvrtina.setEnabled(!bCetvrtinaUsed);

        //Make sure pause is not active
        pauseTimerForAnswerShowing = false;
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {

        finish();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();

            finish();
        }
        super.onDestroy();
    }
}
