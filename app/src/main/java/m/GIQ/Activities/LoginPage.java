package m.GIQ.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import m.GIQ.App;
import m.GIQ.R;

/**
 * Created by mersad on 16.8.2017.
 */

public class LoginPage extends Activity {
    CallbackManager callbackManager;

    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_page);

        callbackManager = CallbackManager.Factory.create();
        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent = new Intent(LoginPage.this, FirstActivity.class);
                startActivity(intent);
                loginResult.getAccessToken();
                finish();
            }

            @Override
            public void onCancel() {
                Log.d("fb", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("fb", "error");
            }
        });

        final Button guestLogin = (Button) findViewById(R.id.guest_login);
        guestLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog builder = new AlertDialog.Builder(LoginPage.this).create();

                builder.setTitle("New guest");
                builder.setMessage("Your name");

                // Set up the input
                final EditText input = new EditText(LoginPage.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setButton(DialogInterface.BUTTON_POSITIVE, "GO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (input.getText().toString().isEmpty())
                                    Toast.makeText(LoginPage.this, "Please enter your name!", Toast.LENGTH_LONG).show();
                                else {
                                    String name = input.getText().toString();

                                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString("name", name);

                                    editor.apply();

                                    App.getInstance().name = name;

                                    Intent intent = new Intent(LoginPage.this, FirstActivity.class);
                                    startActivity(intent);
                                }
                            }
                        });

                builder.show();
            }
        });

        new Handler().postDelayed(new Runnable() {

/*
* Showing splash screen with a timer. This will be useful when you
* want to show case your app logo / company
*/

            @Override
            public void run() {
                if (AccessToken.getCurrentAccessToken() != null) {
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {

                                    try {
                                        if (object.has("name") && !object.getString("name").isEmpty()) {
                                            App.getInstance().name = object.getString("name");

                                            finish();
                                        }
                                    } catch (JSONException e) {

                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "name");
                    request.setParameters(parameters);
                    request.executeAsync();

                    Intent intent = new Intent(LoginPage.this, FirstActivity.class);
                    startActivity(intent);
                } else {
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                    String name = sharedPref.getString("name", null);
                    if (name != null) {
                        App.getInstance().name = name;

                        Intent intent = new Intent(LoginPage.this, FirstActivity.class);
                        startActivity(intent);

                        finish();
                    } else {
                        loginButton.setVisibility(View.VISIBLE);
                        guestLogin.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, 3000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}