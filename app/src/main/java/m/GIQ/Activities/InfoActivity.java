package m.GIQ.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import m.GIQ.Database.OnDatabaseDone;
import m.GIQ.Database.QuestionGetter;
import m.GIQ.Items.Question;
import m.GIQ.Items.QuestionRightShowAdapter;
import m.GIQ.R;

/**
 * Created by mersad on 8.1.2017.
 */

public class InfoActivity extends Activity implements OnDatabaseDone<List<Question>> {

    public List<Question> itemList = new ArrayList<>();
    protected RecyclerView recyclerView;
    protected QuestionRightShowAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopage);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new QuestionRightShowAdapter(itemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        new QuestionGetter(QuestionActivity.NETWORK_KEY).execute(this);
    }

    @Override
    public void OnDatabaseGetData(List<Question> data) {
        for (Question item : data)
            itemList.add(item);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}