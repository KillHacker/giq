package m.GIQ;

/**
 * Created by mersad on 28.3.2017.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONObject;

import java.net.URL;

import m.GIQ.Activities.SettingsActivity;
import m.GIQ.Database.OnImageGetListener;


public class Utils {

    public static NotificationManager mManager;

    @SuppressWarnings("static-access")
    public static void generateNotification(Context context) {

        mManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context, SettingsActivity.class);
        Notification notification = new Notification(R.drawable.islamlogo, "Global IslamQ!", System.currentTimeMillis());
        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        //notification.setLatestEventInfo(context, "IslamQ", "it's time for update your knowledge!", pendingNotificationIntent);
        mManager.notify(0, notification);
    }

    public static void getFacebookProfilePicture(String userID, final OnImageGetListener onImageGetListenerListener) {
        Bundle params = new Bundle();
        params.putString("fields", "picture.type(large)");
        new GraphRequest(AccessToken.getCurrentAccessToken(), userID, params, HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (response != null) {
                            try {
                                JSONObject data = response.getJSONObject();
                                if (data.has("picture")) {
                                    final URL profilePicUrl = new URL(data.getJSONObject("picture").getJSONObject("data").getString("url"));
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Bitmap profilePic = BitmapFactory.decodeStream(profilePicUrl.openConnection().getInputStream());
                                                onImageGetListenerListener.OnImageGet(profilePic);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).start();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).executeAsync();
    }
}