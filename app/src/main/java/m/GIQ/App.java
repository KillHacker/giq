package m.GIQ;

import android.app.Application;

/**
 * Created by Enver and mersad on 17.08.2017..
 */

public class App extends Application {
    private static App mInstance;
    public String name;

    public static App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
