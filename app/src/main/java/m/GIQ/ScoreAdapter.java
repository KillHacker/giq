package m.GIQ;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import m.GIQ.Database.OnImageGetListener;
import m.GIQ.Items.Score;

/**
 * Created by Enver on 15.02.2017..
 */

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.MyViewHolder> {

    private List<Score> itemList;

    public ScoreAdapter(List<Score> mItemList) {
        this.itemList = mItemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Score item = itemList.get(position);

        holder.mPlayerName.setText(item.rank + ". " + item.playerName);
        holder.mScore.setText(item.points + "");

        if (item.fbUserId == null)
            holder.mProfileImage.setImageResource(R.drawable.no_profile_image);
        else if (item.profilePic == null)
            item.setOnImageGetListener(new OnImageGetListener() {
                @Override
                public void OnImageGet(Bitmap bitmap) {
                    ScoreAdapter.this.notifyItemChanged(holder.getPosition());
                }
            });
        else
            holder.mProfileImage.setImageBitmap(item.profilePic);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mProfileImage;
        public TextView mPlayerName, mScore;

        public MyViewHolder(View view) {
            super(view);
            mProfileImage = (ImageView) view.findViewById(R.id.image_view);
            mPlayerName = (TextView) view.findViewById(R.id.player_text);
            mScore = (TextView) view.findViewById(R.id.score_view);
        }
    }
}
