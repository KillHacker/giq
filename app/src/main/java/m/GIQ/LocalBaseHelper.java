package m.GIQ;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import m.GIQ.Items.Question;

/**
 * Created by Enver on 12.02.2017..
 */

public class LocalBaseHelper extends SQLiteOpenHelper implements Serializable {

    public LocalBaseHelper(Context context) {
        super(context, "localQuestions.db", null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE questions (id integer, questionText text," +
                " rightAnswer text, false1 text, false2 text, false3 text, difficulty integer, answered integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS questions");
        onCreate(sqLiteDatabase);
    }

    public void addQuestions(List<Question> questions) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        for (int i = 0; i < questions.size(); i++) {
            Question question = questions.get(i);

            String id = question.getId() + "";
            String questionText = question.getQuestionText();
            String rightText = question.getRightText();
            String[] wrongTexts = question.getWrongTexts();
            int difficulty = question.getDifficulty();

            ContentValues values = new ContentValues();
            values.put("id", id);
            values.put("questionText", questionText);
            values.put("rightAnswer", rightText);
            values.put("false1", wrongTexts[0]);
            values.put("false2", wrongTexts[1]);
            values.put("false3", wrongTexts[2]);
            values.put("difficulty", difficulty);
            values.put("answered", 0);

            sqLiteDatabase.insert("questions", null, values);
        }
    }

    public int getLastId() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "Select * FROM questions ORDER BY id DESC;";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        int retVal = 0;

        if (cursor.moveToFirst()) {
            retVal = cursor.getInt(0);
        }

        cursor.close();

        return retVal;
    }

    public List<Question> getLocalQuestions() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        String query = "Select * FROM questions;";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        List<Question> questions = new ArrayList<>();

        while (cursor.moveToNext()) {
            int answered = cursor.getInt(7);
            if (answered == 0) {
                int id = cursor.getInt(0);
                String questionText = cursor.getString(1);
                String rightText = cursor.getString(2);
                String wrongText1 = cursor.getString(3);
                String wrongText2 = cursor.getString(4);
                String wrongText3 = cursor.getString(5);
                int difficulty = cursor.getInt(6);
                Question question = new Question(id, questionText, rightText, wrongText1, wrongText2, wrongText3, difficulty);
                questions.add(question);
            }
        }

        cursor.close();

        return questions;
    }

    public void setQuestionAnswered(int id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("answered", 1);

        sqLiteDatabase.update("questions", values, "id = " + id, null);
    }

    public void ClearBase() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.delete("questions", null, null);
    }
}
