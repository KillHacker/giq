package m.GIQ.Database;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import m.GIQ.Items.Question;

/**
 * Created by Enver on 16.02.2017..
 */

public class QuestionGetter extends DataBaseHelper<Question> {
    private int fromId;

    public QuestionGetter(String networkKey, int fromId) {
        super(networkKey);
        this.fromId = fromId;
    }

    public QuestionGetter(String networkKey) {
        super(networkKey);
    }

    String DataToSend() {
        return "?" + "action" + "=" + "getQuestions"
                + "&" + "key" + "=" + networkKey
                + "&" + "fromId" + "=" + fromId;
    }

    Question ObjectToData(JSONObject jsonObject, int index) {
        try {
            int id = jsonObject.getInt("id");
            String questionText = jsonObject.getString("questionText");
            String rightText = jsonObject.getString("rightAnswer");
            String wrongText1 = jsonObject.getString("false1");
            String wrongText2 = jsonObject.getString("false2");
            String wrongText3 = jsonObject.getString("false3");
            int difficulty = jsonObject.getInt("difficulty");
            return new Question(id, questionText, rightText, wrongText1, wrongText2, wrongText3, difficulty);
        } catch (JSONException e) {
            Log.e("Parse", "Error parsing questions!");
        }

        return null;
    }
}
