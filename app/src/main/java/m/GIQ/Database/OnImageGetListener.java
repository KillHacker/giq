package m.GIQ.Database;

import android.graphics.Bitmap;

/**
 * Created by Mersad on 9/11/2017.
 */

public interface OnImageGetListener {

    void OnImageGet(Bitmap bitmap);
}
